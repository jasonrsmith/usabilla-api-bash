#!/bin/bash

go get bitbucket.org/jasonrsmith/usabilla-api
git clone git@bitbucket.org:jasonrsmith/usabilla-frontend.git
CURRENTDIR=$(pwd)
go run $GOPATH/src/bitbucket.org/jasonrsmith/usabilla-api/usabilla-api.go --directory="$CURRENTDIR/usabilla-frontend"
